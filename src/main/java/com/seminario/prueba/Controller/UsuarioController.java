package com.seminario.prueba.Controller;


import com.seminario.prueba.Model.UsuarioModel;
import com.seminario.prueba.Service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
@Api(tags = "usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;


    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Usuario", response = UsuarioModel.class)  //esto es un comentario
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public UsuarioModel insertUsuario(@RequestBody UsuarioModel usuarioModel) {
        return usuarioService.saveUsuario(usuarioModel);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Usuario", response = UsuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public UsuarioModel updateCasino(@RequestBody UsuarioModel usuarioModel) {
        return usuarioService.editUsuario(usuarioModel);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Casino", response = UsuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Usuario doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void removeUsuario(@RequestParam Long id) {
        usuarioService.deleteUsuarioById(id);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Casino", response = UsuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Usuario doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<UsuarioModel> getAllUsuario() {
        return usuarioService.getAllUsuario();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get Casino by Id", response = UsuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The BranchOffice doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Optional<UsuarioModel> getByIdUsuario(@RequestParam Long id) {
        return usuarioService.getById(id);
    }

    @PostMapping(path = "/login")
    @ApiOperation(value = "Get Usuario by login data", response = UsuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The BranchOffice doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public UsuarioModel findByLogin(@RequestBody UsuarioModel usuarioModel) {
        return usuarioService.findByLogin(usuarioModel.getCorreo(), usuarioModel.getPassword());
    }
}
