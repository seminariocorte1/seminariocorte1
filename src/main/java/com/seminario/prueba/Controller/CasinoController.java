package com.seminario.prueba.Controller;


import com.seminario.prueba.Model.CasinoModel;
import com.seminario.prueba.Service.CasinoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/casino")
@Api(tags = "casino")
public class CasinoController {

    @Autowired
    private CasinoService casinoService;
    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Casino", response = CasinoModel.class)  //esto es un comentario
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public CasinoModel insertCasino(@RequestBody CasinoModel casino) {
        return casinoService.saveCasino(casino);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Casino", response = CasinoModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public CasinoModel updateCasino(@RequestBody CasinoModel casino) {
        return casinoService.editCasino(casino);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Casino", response = CasinoModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Casino doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void removeCasino(@RequestParam Long id) {
        casinoService.deleteCasinoById(id);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Casino", response = CasinoModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Casino doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<CasinoModel> getAllCasino() {
        return casinoService.getAllCasino();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get Casino by Id", response = CasinoModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The BranchOffice doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Optional<CasinoModel> getByIdCasino(@RequestParam Long id) {
        return casinoService.getById(id);
    }
}


