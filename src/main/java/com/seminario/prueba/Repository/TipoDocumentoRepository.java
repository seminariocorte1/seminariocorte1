package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.TipoDocumentoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumentoModel, Long> {
}
