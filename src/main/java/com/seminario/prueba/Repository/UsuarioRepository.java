package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository <UsuarioModel,Long> {
    UsuarioModel findByLogin(String correo, String password);
}
