package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.RolModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolRepository extends JpaRepository<RolModel, Long> {
}
