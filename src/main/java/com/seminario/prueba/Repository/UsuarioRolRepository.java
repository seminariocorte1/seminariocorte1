package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.UsuarioRolModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRolRepository extends JpaRepository <UsuarioRolModel, Long> {
}
