package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.CasinoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CasinoRepository extends JpaRepository <CasinoModel, Long> {
}
