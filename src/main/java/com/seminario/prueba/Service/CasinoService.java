package com.seminario.prueba.Service;


import com.seminario.prueba.Model.CasinoModel;
import com.seminario.prueba.Repository.CasinoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CasinoService {
    @Autowired
    private CasinoRepository casinoRepository;

    public CasinoModel saveCasino(CasinoModel casino) {
        return casinoRepository.save(casino);
    }

    public CasinoModel editCasino(CasinoModel casino) {
        return casinoRepository.save(casino);
    }

    public void deleteCasinoById(Long id) {
        casinoRepository.deleteById(id);
    }

    public List<CasinoModel> getAllCasino() {
        return casinoRepository.findAll();
    }

    public Optional<CasinoModel> getById(Long id) {
        return casinoRepository.findById(id);
    }
}
