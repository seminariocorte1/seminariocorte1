package com.seminario.prueba.Service;

import com.seminario.prueba.Model.UsuarioModel;
import com.seminario.prueba.Repository.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public UsuarioModel saveUsuario(UsuarioModel usuarioModel) {
        return usuarioRepository.save(usuarioModel);
    }

    public UsuarioModel editUsuario(UsuarioModel usuarioModel) {
        return usuarioRepository.save(usuarioModel);
    }

    public void deleteUsuarioById(Long id) {
        usuarioRepository.deleteById(id);
    }

    public List<UsuarioModel> getAllUsuario() {
        return usuarioRepository.findAll();
    }

    public Optional<UsuarioModel> getById(Long id) {
        return usuarioRepository.findById(id);
    }

    public UsuarioModel findByLogin(String correo, String password) {
        return usuarioRepository.findByLogin(correo, password);
    }

}
