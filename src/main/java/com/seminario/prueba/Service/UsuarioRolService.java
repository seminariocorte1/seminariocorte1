package com.seminario.prueba.Service;


import com.seminario.prueba.Model.UsuarioRolModel;
import com.seminario.prueba.Repository.RolRepository;
import com.seminario.prueba.Repository.UsuarioRolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UsuarioRolService {

    @Autowired
    private UsuarioRolRepository usuarioRolRepository;

    @Autowired
    private RolRepository rolRepository;

    public UsuarioRolModel saveRolUsuario(UsuarioRolModel usuarioRolModel) {
        return usuarioRolRepository.save(usuarioRolModel);
    }

    public UsuarioRolModel editRolUsuario(UsuarioRolModel usuarioRolModel) {
        return usuarioRolRepository.save(usuarioRolModel);
    }

    public void deleteUsuarioRolById(Long id) {
        usuarioRolRepository.deleteById(id);
    }

    public List<UsuarioRolModel> getAllUsuarioRol() {
        return usuarioRolRepository.findAll();
    }

}
