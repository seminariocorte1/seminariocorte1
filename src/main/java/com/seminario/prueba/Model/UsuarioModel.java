package com.seminario.prueba.Model;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "usuario")
@NamedNativeQuery(name = "UsuarioModel.findByLogin", query = "select * from usuario where usuario_correo = ? and usuario_password = ?", resultClass = UsuarioModel.class)
public class UsuarioModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuario_id")
    private Long id;

    @Column(name = "usuario_login")
    private String login;

    @Column(name = "usuario_password")
    private String password;

    @Column(name = "usuario_activo")
    private Boolean activo;

    @Column(name = "usuario_correo")
    private String correo;

    @Column(name = "usuario_correo_verificado")
    private String verificado;

    @Column(name = "usuario_creado_en")
    private Instant creado;

    @Column(name = "usuario_actualizado_en")
    private Instant actualizado;

    public UsuarioModel() {
    }

    public UsuarioModel(Long id, String login, String password, Boolean activo, String correo, String verificado, Instant creado, Instant actualizado) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.activo = activo;
        this.correo = correo;
        this.verificado = verificado;
        this.creado = creado;
        this.actualizado = actualizado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getVerificado() {
        return verificado;
    }

    public void setVerificado(String verificado) {
        this.verificado = verificado;
    }

    public Instant getCreado() {
        return creado;
    }

    public void setCreado(Instant creado) {
        this.creado = creado;
    }

    public Instant getActualizado() {
        return actualizado;
    }

    public void setActualizado(Instant actualizado) {
        this.actualizado = actualizado;
    }
}