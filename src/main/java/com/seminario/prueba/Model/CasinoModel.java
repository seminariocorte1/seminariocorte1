package com.seminario.prueba.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "casino")
public class CasinoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "casino_id")
    private Long id;

    @Column(name = "casino_name")
    private String name;

    @Column(name = "casino_capacity")
    private String capacity;

    @Column(name = "casino_phoneNumber")
    private String phoneNumber;

    @Column(name = "casino_address")
    private String address;

    @Column(name = "casino_city")
    private String city;

    public CasinoModel() {
    }

    public CasinoModel(Long id, String name, String capacity, String phoneNumber, String address, String city) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
