package com.seminario.prueba.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_rol")
public class UsuarioRolModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usrol_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "fk_usuario_id", referencedColumnName = "usuario_id")
    private UsuarioModel usuario;

    @OneToOne
    @JoinColumn(name = "fk_rol_id", referencedColumnName = "rol_id")
    private RolModel rol;

    public UsuarioRolModel() {
    }
}
